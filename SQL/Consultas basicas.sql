--1.- Seleccione todos los registros--
select * from cervezas_tripel ct 

--2.- Seleccione las columnas nombre, tipo y abv--
select nombre, tipo, abv from cervezas_tripel ct 

--3.- Seleccione las columnas cervecería, ciudad y país--
select cerveceria , ciudad, pais from cervezas_tripel ct 

--4.- Seleccione todos los registros de las cervecerías Belgas--
select nombre from cervezas_tripel ct where pais = 'Belgica' 

--5.- Seleccione todos los registros solamente las cervecerías Belgas de la ciudad de Chimay--
select nombre from cervezas_tripel ct where pais = 'Belgica' and ciudad = 'Chimay' 

--6.- Seleccione todos los registros solamente las cervecerías Belgas con tipo de cerveza Belgian 
Dubbel--
select nombre from cervezas_tripel ct where pais = 'Belgica' and ciudad = 'Chimay' and tipo = 'Belgian Dubbel'

--7.- Seleccione nombre, tipo, cervecería, fecha de creación,ciudad y país solamente las cervecerías 
Belgas con tipo de cerveza Belgian Dubbel y con un grado de alcohol mayor o igual a ”7.5”--
select nombre, tipo, cerveceria, creada, ciudad, pais from cervezas_tripel ct where pais ='Belgica' and tipo = 'Belgian Dubbel' and abv >= 7.5

--8.- Seleccione nombre, tipo, cervecería, ciudad y país todas las cervezas que tengan fecha de 
creación ordenadas por cerveceria, fecha de creación y tipo--
select nombre, tipo, cerveceria, ciudad, pais from cervezas_tripel ct where creada <> '' order by cerveceria, creada, tipo -- <>'' o != -->esto es distinto de vacio

--9.- Seleccione todas las cervezas que tengan fecha de creación con el tipo de cerveza Belgian Tripel 
y que su fecha de creación sea del año 2020--
select nombre from cervezas_tripel ct where tipo = 'Belgina Tripel' and creada = '2020'

--10.- Seleccione solamente las cervecerías Holandesas y con un grado de alcohol entre los 8 y 10 
grados ordenadas por nombre y tipo de manera descendente--
select cerveceria from cervezas_tripel ct where pais = 'Holanda' and abv >= 8 and abv <=10 order by nombre, tipo desc

--11.- Seleccione todas las cervecerías--
select cerveceria from cervezas_tripel ct 

--12.- Seleccione todas las cervecerías ocupando el operador DISTINCT--
select distinct cerveceria from cervezas_tripel ct
